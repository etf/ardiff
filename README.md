ardiff - computes A/R difference between two SAM3 profiles/instances

    # python ardiff.py --help
    usage: ardiff.py [-h] [--version] [-c CUT] [-d DAYS] [--prod-link PROD_LINK]
                     [--qa-link PRE_LINK] [--prod-profile PROD_PROF]
                     [--qa-profile PRE_PROF]
    
    ardiff - Computes difference between two SAM3 profiles
    
    optional arguments:
      -h, --help            show this help message and exit
      --version             show program's version number and exit
      -c CUT, --cut CUT     Cutoff rate for A/R
      -d DAYS, --days DAYS  Time period for which the difference will be computed
                            in days (defaults to 1)
      --prod-link PROD_LINK
                            Hostname of SAM3 instance (will be referred to as
                            production
      --qa-link PRE_LINK    Hostname of SAM3 instance (will be referred to as QA
      --prod-profile PROD_PROF
                            A/R profile to query on production instance
      --qa-profile PRE_PROF
                            A/R profile to query on QA instance

    Example:
    $ python /usr/local/sbin/ardiff.py --prod-link wlcg-sam-atlas.cern.ch --qa-link wlcg-sam-atlas-dev.cern.ch 
                                       --prod-profile ATLAS_CRITICAL --qa-profile ATLAS_CRITICAL_ETF